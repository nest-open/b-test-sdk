import { getParsedEthersError } from '@enzoferey/ethers-error-parser';
export const parseEthersError = (err: any) => {
  const parsedEthersError = getParsedEthersError(err);
  let reason;
  if (parsedEthersError.errorCode == 'UNKNOWN_ERROR') {
    const match = err?.error?.reason.match(/execution reverted: (.*)/);
    reason = match ? match[1] : err?.error?.reason;
    if (!reason) {
      reason = parsedEthersError.context;
    }
  } else {
    reason = parsedEthersError.errorCode;
  }
  const message = `Error On Blockchain : ${reason}`;
  return message;
};
