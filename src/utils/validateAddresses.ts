import { ethers } from 'ethers';

export default (addresses: string | string[]) => {
  if (Array.isArray(addresses)) {
    addresses.forEach((address) => {
      if (!ethers.utils.isAddress(address)) throw new Error('Invalid Address');
    });
  } else if (!ethers.utils.isAddress(addresses)) throw new Error('Invalid Address');
};
