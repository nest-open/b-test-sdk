import { ethers } from 'ethers';

export const toEther = (weiAmount: ethers.BigNumberish): string => {
  return ethers.utils.formatEther(weiAmount);
};

export const toWei = (etherAmount: string): string => {
  return ethers.utils.parseUnits(etherAmount, 'ether').toString();
};
