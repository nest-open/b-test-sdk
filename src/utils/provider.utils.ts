import axios from 'axios';
import { ENV } from '../types';
import { BC_STACK_URL } from './constants';
import { ethers } from 'ethers';
const fetchRPC = async (env: ENV): Promise<string> => {
  try {
    const result = await axios.post(BC_STACK_URL[env], {
      type: 'fetchRPCs',
    });
    const rpcURL = result?.data?.result?.status?.rpcs?.EVM.VLRY;
    if (!rpcURL) throw new Error('Error in BC Service');
    return rpcURL as string;
  } catch (error: any) {
    throw new Error(`Error fetching RPCS ${error.message}`);
  }
};

export const getProviders = async (env: ENV): Promise<ethers.providers.JsonRpcProvider> => {
  try {
    const rpcUrl = await fetchRPC(env);
    return new ethers.providers.JsonRpcProvider(rpcUrl);
  } catch (error) {
    throw error;
  }
};

