import { BC_STACK_URL } from './constants';
import { ENV } from '../types';
import axios from 'axios';

export const fetchABIs = async (env: ENV) => {
  try {
    const result = await axios.post(BC_STACK_URL[env], {
      type: 'fetchABIs',
    });
    const ABI = result.data.result.status.abi;
    if (!ABI) throw new Error('ERROR IN BC SERVICES');
    return {
      NFTABI : ABI.BANKSYNFT
    };
  } catch (error) {
    throw new Error(`Error fetching ABIS ${(error as Error).message}`);
  }
};


